/**
 * Created by jaustin on 9/14/14.
 */
var App = (function ($, Ember, siteTemplates, maasive) {
    var App;
    $.support.cors = true;
    if (debugApp) {
        App = Ember.Application.create({
            rootElement: '#maasive-app',
            LOG_TRANSITIONS: true,
            LOG_TRANSITIONS_INTERNAL: true
        });
    } else {
        App = Ember.Application.create({
            rootElement: '#maasive-app'
        });
    }
    App.env = window.siteEnv || {};
    App.Router.map(function () {
        this.route("index", {
            path: "/"
        });
    });
    App.ApplicationRoute = Ember.Route.extend({
        beforeModel: function () {
            var promises = [];
            siteTemplates.forEach(function (item) {
                if (!Ember.TEMPLATES[item]) {
                    promises.push(
                        $.get('templates/' + item.replace('/', '.') + '.hbs').then(
                            function (data) {
                                Ember.TEMPLATES[item] = Ember.Handlebars.compile(data);
                            })
                    );
                }
            });

            return Ember.RSVP.all(promises);
        },
        model: function () {
            var promises = {
                currentUser: maasive.get('/auth/user/')
                    .then(function (resp) {
                        return Ember.Object.create(resp.data[0]);
                    }, function () {
                        return null;
                    })
            };
            return Ember.RSVP.hash(promises).then(function (result) {
                return Ember.Object.create(result);
            });
        },
        actions: {
            login: function (email, password) {
                var self = this;
                maasive.post('/auth/login/', {
                    email: email,
                    password: password
                })
                    .then(function (resp) {
                        self.refresh();
                    }, function (error) {
                        self.transitionTo('error');
                    });
            },
            logout: function () {
                maasive.auth.logout(function () {
                    App.set('currentUser', null);
                    App.set('managedApi', null);
                });
                this.transitionTo('index');
            }

        }
    });
    App.IndexRoute = Ember.Route.extend({
        setupController: function (controller, model) {
            controller.set('model', model);
            controller.entriesRefresh();
            controller.optInsRefresh();
        }
    });
    App.NavController = Ember.Controller.extend({
        needs: ['application'],
        currentUser: Ember.computed.alias('controllers.application.model.currentUser')
    });
    App._fetchCollection = function (options, offset) {
        var offsetString = '';
        var percent, response;
        console.log('_fetchCollection');
        if (offset) {
            offsetString = '&offset=' + offset;
        }
        return maasive.get('/' + options.name + '/?limit=5000' + offsetString)
            .then(function (resp) {
                if (resp.data.length > 0) {
                    response = App._fetchCollection(options, resp.data[resp.data.length - 1]['id']);
                } else {
                    response = options;
                }
                options.set('fetched', options.get('fetched') + resp.data.length);
                percent = options.get('fetched') / options.get('count') * 100;
                options.set('percent', percent.toFixed(1));
                options.parseContent(options, resp.data);
                return response;
            })
    };
    App.fetchCollection = function (options) {
        console.log('fetchCollection');
        return maasive.options('/' + options.name + '/')
            .then(function (resp) {
                options.set('count', resp.data['resource_count']);
                return App._fetchCollection(options);
            });

    };
    App.parseEntries = function (options, content) {
        var item, country;
        while (content.length > 0) {
            item = content.shift();
            options.unique.add(item.email);
            country = (item.country || 'us').toLowerCase();
            if (options.countries.contains(country)) {
                options.countByCountry[country]++;
            } else {
                options.countries.add(country);
                options.countByCountry[country] = 1;
            }
        }
        options.set('entriesByCountry', []);
        options.countries.forEach(function (i) {
            options.entriesByCountry.addObject({
                name: i,
                count: options.countByCountry[i]
            })
        });
        options.entriesByCountry.sort(function (a, b) {
            if (a.name > b.name) {
                return 1;
            }
            if (a.name < b.name) {
                return -1;
            }
            // a must be equal to b
            return 0;
        });

    };
    App.parseOptIns = function (options, content) {
        var item, optIn;
        while (content.length > 0) {
            item = content.shift();
            while (item.opt_ins.length > 0) {
                optIn = item.opt_ins.shift().toLowerCase();
                if (options.optIns.contains(optIn)) {
                    options.countByName[optIn]++;
                } else {
                    options.optIns.add(optIn);
                    options.countByName[optIn] = 1;
                }
            }
        }
        options.optIns.remove('shareinfo');
        options.set('optInsByName', []);
        options.optIns.forEach(function (i) {
            options.optInsByName.addObject({
                name: i,
                count: options.countByName[i]
            })
        });
        options.optInsByName.sort(function (a, b) {
            if (a.name > b.name) {
                return 1;
            }
            if (a.name < b.name) {
                return -1;
            }
            // a must be equal to b
            return 0;
        });

    };
    App.IndexController = Ember.Controller.extend({
        needs: ['application'],
        currentUser: Ember.computed.alias('controllers.application.model.currentUser'),
        entries: null,
        entriesLoading: null,
        entriesRefresh: function () {
            var self = this;
            var options = Ember.Object.create({
                name: 'entries',
                content: [],
                count: 0,
                fetched: 0,
                percent: 0,
                unique: new Ember.Set(),
                countries: new Ember.Set(),
                countByCountry: {},
                entriesByCountry: [],
                parseContent: App.parseEntries
            });
            self.set('entries', options);

            if (self.get('currentUser')) {
                self.set('entriesLoading', true);
                App.fetchCollection(options).then(function () {
                    self.set('entriesLoading', null);
                });
            }
        },
        optIns: null,
        optInsLoading: null,
        optInsRefresh: function () {
            var self = this;
            var options = Ember.Object.create({
                name: 'opt_ins',
                content: [],
                count: 0,
                fetched: 0,
                percent: 0,
                optIns: new Ember.Set(),
                countByName: {},
                optInsByName: [],
                parseContent: App.parseOptIns
            });
            self.set('optIns', options);
            if (self.get('currentUser')) {
                self.set('optInsLoading', true);
                App.fetchCollection(options).then(function () {
                    self.set('optInsLoading', null);
                });
            }
        }
    });
    Ember.Handlebars.registerBoundHelper('countByCountry', function (options) {
        var cbc = [];
        options.countries.forEach(function (item) {
            cbc.push('<p>' + item + ': ' + options.countByCountry[item] + '</p>');
        });
        var escaped = Handlebars.Utils.escapeExpression(cbc.join(''));
        return new Ember.Handlebars.SafeString('<div class="">' + escaped + '</div>');
    });
    return App;
}($, Ember, siteTemplates, maasive));
